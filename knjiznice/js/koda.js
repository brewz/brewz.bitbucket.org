
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/*
window.onload = function(){
    var imeAplikacije = "WTFseTukiDoagaja";
    document.getElementById('imeAplikacije').innerHTML = imeAplikacije;
};
*/

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function Dodajalergije(alergije,stPacienta){
		$('#alergije').empty();
		$('#zdravilaa').empty();
		$('#problemi').empty();
		for(var j=0; j<alergije[stPacienta-1].length; j++){
            $('ul.allergies').append('<li>'+alergije[stPacienta-1][j]+ '</li>');
		}
 }
     
    function DodajaZdravila(zdravila,stPacienta){
		for(var j=0; j<zdravila[stPacienta-1].length; j++){
            $('ul.medications').append('<li>'+zdravila[stPacienta-1][j]+ '</li>');
		}
    }
	function DodajProbleme(problems,stPacienta){
		for(var j=0; j<problems[stPacienta-1].length; j++){
            $('ul.problems').append('<li>'+problems[stPacienta-1][j]+ '</li>');
		}
}
function generirajPodatke(stPacienta) {
	var sessionId = getSessionId();
	var ime="";
  	var priimek="";
  	var datumRojstva="";
  	
	
  if(stPacienta==1){
  	ime="Jure";
  	priimek="Kregulj";
    datumRojstva="1998-12-25";
  }
  
  if(stPacienta==2){
  	ime="Joze";
  	priimek="Ocvirek";
    datumRojstva="1960-11-09";
  }
  
  if(stPacienta==3){
  	ime="Drilito";
  	priimek="BolaniDoktor";
    datumRojstva="1980-08-08";
  }
  
  	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	
		                	
		                    $("#Bolnik"+stPacienta).html("<div class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</div>");
		                    $("#preberiEHRid").val(ehrId);
		                    console.log(stPacienta+": "+ehrId);
		                     $('#'+stPacienta).val(ehrId);
		                       if(stPacienta==1){
		                       	dodajMeritveVitalnihZnakov(ehrId,"2000-11-09",175,80,37.1,160,100,98.7,sessionId,"Lekadol");
							  	dodajMeritveVitalnihZnakov(ehrId,"2005-11-09",175,90,37.5,166,90,98,sessionId,"Brihtol");
							  	dodajMeritveVitalnihZnakov(ehrId,"2012-11-09",175,100,37,170,115,100,sessionId,"Bilobil");
							  	dodajMeritveVitalnihZnakov(ehrId,"2015-11-09",175,110,37.9,155,140,97,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2018-11-09",175,120,39,170,120,70,sessionId);
							  	
							  }
							  
							  if(stPacienta==2){
							  	dodajMeritveVitalnihZnakov(ehrId,"2001-10-09",175,80,37.1,120,80,98.7,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2005-04-01",175,90,37.5,122,82,98,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2010-11-09",175,100,37,115,81,100,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2014-07-08",175,110,37.9,130,90,97,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2018-01-01",175,120,39,120,79,99,sessionId);
							  }
							  
							  if(stPacienta==3){
							  		dodajMeritveVitalnihZnakov(ehrId,"2000-11-09",175,80,37.1,150,80,98.7,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2005-11-09",175,90,37.5,120,89,98,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2012-11-09",175,100,37,130,81,100,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2016-06-06",175,110,37.9,125,75,97,sessionId);
							  	dodajMeritveVitalnihZnakov(ehrId,"2018-02-02",175,120,39,130,90,90,sessionId);
							  }
		                     
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	
  	
}


function dodajMeritveVitalnihZnakov(ehrid,datuminura,telesnaV,telesnaT,telesnaTemp,sisTlak,diaTlak,nasicKrvi,sessionId) {
	

	var ehrId = ehrid;
	var datumInUra = datuminura;
	var telesnaVisina = telesnaV;
	var telesnaTeza = telesnaT;
	var telesnaTemperatura = telesnaTemp;
	var sistolicniKrvniTlak = sisTlak;
	var diastolicniKrvniTlak = diaTlak;
	var nasicenostKrviSKisikom = nasicKrvi;
	


	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom,
		    
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                   
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function preberiEHRodBolnika() {
	var sessionId = getSessionId();
	var dates=[];
	var systolicT=[];
	var diastolicT=[];
	var alergije=[["Astma","Laktoza","Gluten"],["Nimate alergij"],["Prah","Sonce"]];
	var zdravila=[["Brihtol x2","Medicinska marihuana 8gs","Aspirin x3"],["Brez"],["Bilobil x1"]];
	var problems=[["Slabi rezultati","Oglasite se pri osebnem zdravniku","Pojdite po zdravila"],["Dobri rezultati"],["Oglasite se zobozdravniku","Pojdite po zdravila"]];
	
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
//_____________________-KISIK BAR-____________________________________
		$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/spO2",
			    type: 'GET',
			    headers: {
			        "Ehr-Session": sessionId
			    },
			    success: function (res) {
			        var value = res[0].spO2.toFixed(2);
			        $('.last-spo2').text(value + "%");
			        $('.bar-spo2').css('width', value +"%");
			    }
			});
//_____________________-KISIK BAR-____________________________________



	var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	$.ajax({
		async:false,
	    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
	    type: 'GET',
	    headers: {
	        "Ehr-Session": sessionId
	    },
	    
	    success: function (res) {
	        res.forEach(function (el, i, arr) {
	            date = new Date(el.time);
	            el.date = date.getTime();
	            console.log(el);
	            console.log(date);
	            console.log(date.getDate() + '-' + monthNames[date.getMonth()]+"-"+date.getFullYear());
	            dates.push(date.getDate() + '-' + monthNames[date.getMonth()]+"-"+date.getFullYear());
	            systolicT.push(el.systolic);
	            diastolicT.push(el.diastolic);
	            
	            console.log(dates);
	            console.log(systolicT);
	        });
	       dates.reverse();
	       systolicT.reverse();
	       diastolicT.reverse();
	       
	       var ctx = document.getElementById('myChart').getContext('2d');
				var chart = new Chart(ctx, {
				    // The type of chart we want to create
				    type: 'line',
				    // The data for our dataset
					data: {
				        labels: dates,
				        datasets: [
				        {
				            label: "systolic",
				            fill:false,
				            lineTension :0.1,
				            borderColor: 'rgb(75, 192, 192)',
				            data: systolicT,
				            backgroundColor: 'rgb(75, 192, 192)',
				        },
				        {
				        	label: "diastolic",
				            fill:false,
				            lineTension :0.1,
				            borderColor: 'rgb(75, 19, 75)',
				            data: diastolicT,
				            backgroundColor: 'rgb(75, 19, 75)',
				        }]
				       
				            
				    },
				    // Configuration options go here
				    options: {}
			});
	    }
	});
	
	var izbrani= document.getElementById("preberiObstojeciEHR");
	console.log(izbrani.options.selectedIndex);
	
	Dodajalergije(alergije,izbrani.options.selectedIndex);
	
    
    DodajaZdravila(zdravila,izbrani.options.selectedIndex);
		
	DodajProbleme(problems,izbrani.options.selectedIndex);
		

	
	
	}
	
}


function getEHR(stPacienta){
	var vrstica=document.getElementById("Bolnik"+stPacienta);
	var besedilo = vrstica.textContent;
	console.log(besedilo);
}
//___________________________-GOOGLE API-________________________________
var infoWindow, pos;
var ustanova = "";
var image="/brewz.bitbucket.org/icons/hospital-building.png";
var service;

var lj = {lat: 46.050674, lng: 14.502896};
      function initMap() {
      	
      	
        map = new google.maps.Map(document.getElementById('map'), {
          center: lj,
          zoom: 11
        });
        infoWindow = new google.maps.InfoWindow;
        

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			
            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Location found.');
            //infoWindow.open(map);
            map.setCenter(pos);
         
            	service = new google.maps.places.PlacesService(map);
            
		        
            
        	
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

		function prikaziBolnisnice(){
			map = new google.maps.Map(document.getElementById('map'), {
	          center: pos,
	          zoom: 13
	        });
			ustanova="hospital";
			image="https://bitbucket.org/brewz/brewz.bitbucket.org/raw/master/icons/hospital-building.png";
			service.nearbySearch({
		          location: pos,
		          
		          radius: 2500,
		          type: [ustanova]
		        }, callback);
		        console.log(pos);
		        console.log(ustanova);
				}
		function prikaziLekarne(){
			map = new google.maps.Map(document.getElementById('map'), {
	          center: pos,
	          zoom: 13
	        });
			ustanova="pharmacy";
			image="https://bitbucket.org/brewz/brewz.bitbucket.org/raw/master/icons/firstaid.png";
			service.nearbySearch({
		          location: pos,
		          
		          radius: 2500,
		          type: [ustanova]
		        }, callback);
		        console.log(pos);
		        console.log(ustanova);
		}
		function prikaziZobarje(){
			map = new google.maps.Map(document.getElementById('map'), {
	          center: pos,
	          zoom: 13
	        });
			ustanova="dentist";
			image="https://bitbucket.org/brewz/brewz.bitbucket.org/raw/master/icons/dentist.png";
			service.nearbySearch({
		          location: pos,
		          
		          radius: 2500,
		          type: [ustanova]
		        }, callback);
		        console.log(pos);
		        console.log(ustanova);
		}
      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
      
      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
        else{
        	console.log("v bližini ni ničesar");
        	infoWindow.setPosition(pos);
        	infoWindow.setContent("Zdravstvenih ustanov ni v bližini");
        	infoWindow.open(map);
        }
      }
      
        function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          placeId: place.place_id,
          map: map,
          position: placeLoc,
          icon: image
        });
        google.maps.event.addListener(marker, 'click', function() {
        	
          infoWindow.setContent('<div><strong>' + place.name + '</strong><br>'
                );
                
          infoWindow.open(map, this);
        });
      }
      
//___________________________-GOOGLE API-________________________________


      



$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

});